﻿namespace SuperdenseCoding
{
    open Microsoft.Quantum.Canon;
    open Microsoft.Quantum.Intrinsic;
    open Microsoft.Quantum.Convert;

    operation Put(desired: Result, q1: Qubit) : Unit {
        if (desired != M(q1)) {
            X(q1);
        }
    }

    operation SuperdenseCodingTest (count : Int, q0_initial: Result, q1_initial: Result, op: Int) : (Int, Int) {

        mutable numCorrectOpDetections = 0;
        using ((q0, q1) = (Qubit(), Qubit())) {

            for (test in 1..count) {
                Put (q0_initial, q0);
                Put (q1_initial, q1);
                
                // Entangle the qubits
                H(q0);
                CNOT(q0, q1);

                // Perfoming one of the 4 operations on q0
                if(op == 0) {
                    I(q0);
                }
                if(op == 1) {
                    X(q0);
                } 
                if(op == 2) {
                    Z(q0);                
                } 
                if(op == 3) {
                    Y(q0);                
                }

                // Unentangling the qubits
                CNOT(q0, q1);
                H(q0);

                // Measuring the qubits
                let q0_res = M(q0);
                let q1_res = M(q1);

                let detected_op = ResultArrayAsInt([q1_res, q0_res]);

                // Count the number of ones we saw:
                if (detected_op == op) {
                    set numCorrectOpDetections += 1;
                }
            }
            Put(Zero, q0);
            Put(Zero, q1);
        }

        // Return number of times we detect the operation correctly and incorrectly.
        return (numCorrectOpDetections, count-numCorrectOpDetections);
    }

}
