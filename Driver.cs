﻿using System;
using System.Linq;

using Microsoft.Quantum.Simulation.Core;
using Microsoft.Quantum.Simulation.Simulators;

namespace SuperdenseCoding
{
    class Driver
    {
        enum Operations {I=0, X=1, Z=2, Y=3};
        static void superdenseCoding(Operations operation) {
            using (var qsim = new QuantumSimulator())
            {
                // Try initial values
                var res = SuperdenseCodingTest.Run(qsim, 1000, Result.Zero, Result.Zero, (int)operation).Result;
                var (correct, incorrect) = res;
                System.Console.WriteLine(
                    $"Number: Correct={correct,-4}, Incorrect={incorrect,-4}");
            }
        }

        static int checkArgs(string[] args) {

            bool validOperation = false;
            if(args.Length == 1 && args[0].Length == 1) {
                
                foreach(var op in Enum.GetNames(typeof(Operations))) {
                    if(args[0].Contains(op)) {
                        validOperation = true;
                        break;
                    }
                }
            }

            if(!validOperation) {
                System.Console.WriteLine("Please specify an operation to perform: 'I', 'X', 'Y' or 'Z'.");
                return 1;
            }

            return 0;
        }

        static int Main(string[] args)
        {
            int err = checkArgs(args);
            if(err != 0) {
                return err;
            }
            
            Operations operation = (Operations)Enum.Parse(typeof(Operations), args[0]);
            System.Console.WriteLine("Operation:" + operation.ToString() + "(" + (int)operation + ").");

            superdenseCoding(operation);

            System.Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
            return 0;
        }
    }
}