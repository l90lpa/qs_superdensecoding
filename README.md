
## SuperdenseCoding

This repo contains a Q# kernel example of superdense coding.

## To build/run

- In a terminal set the working directory to the root level directory of this project.
- run `dotnet run INSERT_ARGUMENT`, where the available arguments are `I`, `X`, `Y` or `Z`.